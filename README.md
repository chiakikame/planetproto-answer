This is a repository of my answers of `planetproto` workshop, for future reference only.

## Important aspects

* `__proto__` and `Object.create`
* Shared fields are shallow-copied.
* Constructor functions (Upper-cased functions) and `new` keyword
* `prototype` field
